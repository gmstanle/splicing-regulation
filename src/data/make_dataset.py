# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import pandas as pd

@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (raw) into
        cleaned data ready to be analyzed (saved in processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()

# TODO: test this 
def get_splicing_factors():

    # Load different data soures on mouse splicing factors
    # The URLs for downloading these are in docs/getting-started.rst
    amigo_RNAspliceReg_mouse = pd.read_table('data/external/AMIGO_mouse_protein_directAnnotation_regulationOfRNAsplicing.txt', 
    header=None, index_col=4)
    amigo_ASregulation_mouse = pd.read_table('data/external/AMIGO_mouse_protein_directAnnotation_regulationOfAS.txt', 
    header=None, index_col=5)
    scheiffele=pd.read_csv('data/external/Scheiffele2019_SF_list.csv', index_col=0, skiprows=2)
    scheiffele['Gene Symbol'] = scheiffele['Gene Symbol'].str.strip()

    # combine these lists into a single list 
    SF_mouse = list(set(amigo_RNAspliceReg_mouse.index.to_list() + 
               amigo_ASregulation_mouse.index.to_list() + 
               scheiffele['Gene Symbol'].to_list()))


    # Get ENSEMBL IDs for these genes
    mouse_genes=pd.read_csv('data/external/mouse_genes_ENSEMBL.csv', index_col=2)
    SF_mouse = mouse_genes.loc[SF_mouse,'Gene stable ID']

    # Write to csv output
    SF_mouse.to_csv('data/processed/mouse_splice_factors.csv')



                                        