from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Investigating splicing regulation in public datasets to make testable predictions for CRISPR/sequencing experiments',
    author='Geoff Stanley',
    license='',
)
