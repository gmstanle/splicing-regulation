Getting started
===============

This is where you describe how to get set up on a clean install, including the
commands necessary to get the raw data (using the `sync_data_from_s3` command,
for example), and then how to make the cleaned, final data sets.

Downloading gene lists
-----------------------
Download lists of splicing factors from these places:
AMIGO_
Scheiffele_

For AMIGO, filter by genes with Direct Annotation of two terms:
"regulation of alternative mRNA splicing, via spliceosome"
"regulation of RNA splicing"
and download as two separate files. mv the files to
``data/external/AMIGO_mouse_protein_directAnnotation_regulationOfRNAsplicing.txt``
``data/external/AMIGO_mouse_protein_directAnnotation_regulationOfAS.txt``
Depending on what column the gene names download as, you will need to change the
column in the code in ``src/data/``

For Scheiffele_ (Supplementary Table 6), export the first sheet to csv and mv to
``data/external/Scheiffele2019_SF_list.csv``

Download mouse gene name - ENSEMBL ID translation from Biomart_

.. _Biomart: http://www.ensembl.org/biomart
.. _AMIGO: http://amigo.geneontology.org/
.. _Scheiffele: https://static-content.springer.com/esm/art%3A10.1038%2Fs41593-019-0465-5/MediaObjects/41593_2019_465_MOESM8_ESM.xlsb