import scanpy as sc
import pandas as pd 
import numpy as np


def subsample_cells_group(adata, n, group_key):
    """\
    Subsample the cells from a scanpy object to a constant number per group.
    For groups with fewer cells than n, do not subsample. 
    
    Parameters
    --------
    adata
        AnnData object
    n
        number of cells to subsample to
    group_key
        group to subsample by

    Returns
    --------
    Subsampled AnnData object

    """
    final_idx_list = pd.Index([]) # this initializes a blank Index
    
    for group in adata.obs[group_key].cat.categories:
        grp_idx = adata.obs.index[adata.obs[group_key]==group]
        
        # only subsample if there's more cells than n_cells
        if len(grp_idx) > n:
            grp_idx_subsampled = grp_idx[np.random.choice(len(grp_idx), n, replace=False)]
        else:
            grp_idx_subsampled = grp_idx
        
        final_idx_list=final_idx_list.append(grp_idx_subsampled)
        
    return(adata[final_idx_list])


def filter_groups_by_ncells(adata, n, group_key):
    """\
    Remove groups with fewer than n_cells cells.
    
    Parameters
    --------
    adata
        AnnData object
    n
        minimum number of cells
    group_key
        key of group to filter on 

    Returns
    --------
    AnnData object

    """
    group_counts = adata.obs[group_key].value_counts()
    groups_retain = group_counts.loc[group_counts > n, ].index    

    return(adata[adata.obs[group_key].isin(groups_retain)])